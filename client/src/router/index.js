import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Products from '@/components/Products'
import Search from '@/components/Search'
import './../assets/css/styles.css'
import 'vue-awesome/icons'
Vue.use(Router)
import Icon from 'vue-awesome/components/Icon'
Vue.component('icon', Icon)

//for courosel
import VueAwesomeSwiper from 'vue-awesome-swiper'
Vue.use(VueAwesomeSwiper)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/products/:id',
      name: 'Products',
      component: Products,
      props: true
    },
    {
      path: '/search/:category',
      name: 'Search',
      component: Search,
      props: true
    }
  ]
});
