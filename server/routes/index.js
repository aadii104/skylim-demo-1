var express = require('express');
var router = express.Router();

var Model = require('./../models/model');

/* GET home page. */
router.get('/', function (req, res) {
    res.json({ success: true, message: 'Hello World' });
});

router.get('/products_all', function (req, res) {
    Model.Product.find({}).exec()
        .then((products) => {
            res.json({ success: true, message: 'All products found', products: products });
        })
        .catch((err) => {
            if (err) {
                console.log(err);
                res.status(500).json({
                    success: false,
                    message: 'Something happened at our end. Check back after sometime'
                });
            }
        });
});

router.get('/vendors_all', function (req, res) {
    Model.Vendor.find({}).exec()
        .then(vendors => {
            res.json({ success: true, message: 'All vendors found', vendors: vendors });
        })
        .catch(err => {
            if (err) {
                console.log(err);
                res.status(500).json({
                    success: false,
                    message: 'Something happened at our end. Check back after sometime'
                });
            }
        });
});

router.get('/product', function (req, res) {
    var dataType = req.query.type;

    if (dataType === 'category') {
        let category = req.query.category;

        Model.Product.find({ category: category }).exec()
            .then(products => {
                res.json({
                    success: true,
                    message: 'Found products matching the category ' + category,
                    products: products
                });
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({
                    success: false,
                    message: 'Something happened at our end. Check back after sometime'
                });
            });
    } else if (dataType === 'price') {
        let minPrice = parseInt(req.query.min);
        let maxPrice = parseInt(req.query.max);

        Model.Product.find({ price: { $gte: minPrice, $lte: maxPrice } }).exec()
            .then(products => {
                res.json({
                    success: true,
                    message: 'Found products matching the price range',
                    products: products
                });
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({
                    success: false,
                    message: 'Something happened at our end. Check back after sometime'
                });
            });
    } else if (dataType === 'vendor') {
        let vendorId = req.query.vendor;

        Model.Product.find({ vendorId: vendorId }).exec()
            .then(products => {
                res.json({
                    success: true,
                    message: 'Found all products of the vendor ' + vendorId,
                    products: products
                });
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({
                    success: false,
                    message: 'Something happened at our end. Check back after sometime'
                });
            });
    } else {
        let productId = parseInt(req.query.product);

        Model.Product.findOne({ productId: productId }).exec()
            .then(product => {
                if (product)
                    res.json({
                        success: true,
                        message: 'Found the product',
                        products: product
                    });
                else
                    res.json({ success: false, message: 'No matching product available' });
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({
                    success: false,
                    message: 'Something happened at our end. Check back after sometime'
                });
            });
    }
});

router.get('/vendor', function (req, res) {
    var dataType = req.query.type;

    if (dataType === 'rating') {
        let rating = req.query.rating;

        Model.Vendor.find({ rating: rating }).exec()
            .then(vendors => {
                res.json({
                    success: true,
                    message: 'Found all vendors matching the rating ' + rating,
                    vendors: vendors
                });
            })
            .catch(err => {
                if (err) {
                    console.log(err);
                    res.status(500).json({
                        success: false,
                        message: 'Something happened at our end. Check back after sometime'
                    });
                }
            });
    } else {
        let vendorId = req.query.vendor;

        Model.Vendor.findOne({ vendorId: vendorId }).exec()
            .then(vendor => {
                if (vendor)
                    res.json({
                        success: true,
                        message: 'Found the vendor',
                        vendors: vendor
                    });
                else
                    res.json({ success: false, message: 'No matching vendor found' });
            })
            .catch(err => {
                if (err) {
                    console.log(err);
                    res.status(500).json({
                        success: false,
                        message: 'Something happened at our end. Check back after sometime'
                    });
                }
            });
    }
});

module.exports = router;
