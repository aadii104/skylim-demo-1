var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/TestDatabase');
var db = mongoose.connection;
db.on('error', function (err) {
    console.log(err);
});
db.on('disconnected', function () {
    console.log('Database Disconnected');
});

var Model = require('./model');
var fs = require('fs');

var vendors = JSON.parse(fs.readFileSync('./../database/db.json')).vendors;
console.log('Loaded Vendors');
var products = JSON.parse(fs.readFileSync('./../database/db.json')).products;
console.log('Loaded Products');

var vendorsArray = [];
var productsArray = [];
db.on('connected', function () {
    for (let i = 0; i < vendors.length; i++) {
        let promiseObject = new Model.Vendor({
            vendorId: vendors[i].vendor_id,
            reviews: vendors[i].reviews,
            rating: vendors[i].rating,
            address: vendors[i].address,
            range: vendors[i].distance,
            name: vendors[i].name,
            image: vendors[i].image
        }).save();
        vendorsArray.push(promiseObject);
    }
    Promise.all(vendorsArray)
        .then(() => {
            console.log('Successfully Stored All');
        })
        .catch((error) => {
            console.log(error);
        });

    for (let i = 0; i < products.length; i++) {
        let promiseObject = new Model.Product({
            productId: products[i].product_id,
            vendorId: products[i].vendor_id,
            name: products[i].name,
            category: products[i].category,
            image: products[i].image,
            price: products[i].price,
            rating: products[i].rating,
            stock: products[i].stock,
            description: products[i].description
        }).save();
        productsArray.push(promiseObject);
    }
    Promise.all(productsArray)
        .then(() => {
            console.log('Successfully Stored All');
        })
        .catch((error) => {
            console.log(error);
        });
});