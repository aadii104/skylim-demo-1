var mongoose = require('mongoose');

var Vendor = mongoose.Schema({
    vendorId: {
        type: Number,
        required: true,
        index: true
    },
    reviews: {
        type: String,
        required: true
    },
    rating: {
        type: Number,
        required: true,
        index: true
    },
    address: {
        type: String,
        required: true
    },
    range: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    }
});

var Product = mongoose.Schema({
    productId: {
        type: String,
        required: true,
        index: true
    },
    vendorId: {
        type: String,
        required: true,
        index: true
    },
    name: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true,
        index: true
    },
    image: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true,
        index: true
    },
    rating: {
        type: Number,
        required: true
    },
    stock: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: true
    }
});

module.exports = {
    Vendor: mongoose.model('vendor', Vendor),
    Product: mongoose.model('product', Product)
};